=======================================================================================
arch-mkchroot
=======================================================================================

---------------------------------------------------------------------------------------
Create and configure an Arch Linux chroot environment.
---------------------------------------------------------------------------------------

:Author:         Stefan Göbel < archlxc ʇɐ subtype ˙ de >
:Date:           2018/11/20
:Version:        0.2
:Manual section: 8
:Manual group:   Admin Commands

SYNOPSIS
=======================================================================================

**arch-mkchroot** *[options]* *<packages>*

DESCRIPTION
=======================================================================================

The **arch-mkchroot** script may be used to bootstrap and configure an Arch Linux
chroot environment. The configuration is performed based on command line parameters and
the configuration of the host system: **arch-mkchroot** will check all files in the
chroot's `/etc/` directory and sync them with the host. Files that are missing on the
host will also be deleted from the chroot if they belong to a package installed on both
the host and in the chroot. This behaviour can be controlled by the appropriate command
line paramaters, see below. Note that by default certain files are excluded, including
the `passwd`/`group`/`shadow`/`gshadow` and other system specific files.

Note: Only regular files (i.e. no symlinks), on both the host and in the chroot, will
be synced!

There are also options to create directories in the chroot, copy additional files or
directories to the chroot, delete files or directories in the chroot or change the
owner of files or directories. These operations will be performed in this order after
basic configuration and syncing.

OPTIONS
=======================================================================================

Note: For options that may be specified multiple times, subsequent calls will add the
argument to the list of previous values. These options may also have default values,
and values may be added in a config file. To reset one of these options to an empty
list, assign an empty value, e.g. use `--exclude ""` to empty the list of excluded
packages. This will clear all default and previously assigned values.

Unless stated otherwise, if an option is used multiple times the last value will
override previous values.

**-a**, **--enable-service** *<service>*

   Enable the specified systemd service in the chroot. May be specified multiple times.
   No services will be enabled by default.

**-A**, **--disable-service** *<service>*

   Disable the specified systemd service in the chroot. May be specified multiple
   times. No services will be disabled by default.

**-b**, **--multilib**

   Do not remove the `multilib` repository from the chroot's `pacman.conf`.

**-B**, **--no-multilib**

   Remove the `multilib` repository from the chroot's `pacman.conf` if it is present.
   This is the default setting.

**-c**, **--cp** *<source[:<target>]>*

   Copy this file (specified by its full path) from the host to the chroot. The file
   will have the same path and name in the chroot as on the host, unless the file name
   is followed by `:<target>`, in which case this will be the path/name inside the
   chroot (it must also be a full path, relative to the chroot directory). If a target
   path is specified, parent directories have to be created manually (see *--mkdir*).
   This option may be specified multiple times, no files are included by default.

**-C**, **--no-cp** *<file>*

   Do not sync this file from the host to the chroot, but still delete it if it has
   been deleted from the host. Files must be specified relative to the `/etc/`
   directory. This option may be specified multiple times, and it may contain shell
   glob patterns. See also *--skip*. No files are included in this list by default.

**-d**, **--directory** *<directory>*

   Root directory of the new chroot. Defaults to `.`, i.e. the current directory. See
   also *--check-empty* and *--check-mount*.

**-D**, **--skip** *<file>*

   Completely skip this file when syncing, i.e. do not copy it from the host, do not
   delete it if it has been deleted on the host. Paths must be relative to the `/etc/`
   directory. This option may be specified multiple times, and it may contain shell
   glob patterns. By default, the following files will be skipped: `fstab`, `mtab`,
   `group`, `gshadow`, `passwd`, `shadow`, `hostname`, `machine-id`, `hosts` and
   `ld.so.cache`.

**-e**, **--etcgit**

   Install and configure etcgit (this is a custom package not available in any official
   repository). Do not use this option unless you know what you're doing!

**-E**, **--no-etcgit**

   Do not install etcgit (see above). This is the default.

**-f**, **--config** *<file>*

   Read parameters from this configuration file. See the *CONFIGURATION FILE* section
   below for more information. No configuration file will be used by default.

**-F**, **--pacman-config** *<file>*

   Use this `pacman` configuration file instead of `/etc/pacman.conf`.

**-g**, **--copy-keyring**

   Copy the `pacman` keyring from the host to the chroot.

**-G**, **--no-copy-keyring**

   Do not copy the host's `pacman` keyring to the chroot, generate a new one instead.
   This is the default.

**-h**, **--help**

   Show the help for the **arch-mkchroot** command.

**-H**, **--dump-config**

   If this option is specified, the command line options, configuration files and
   package list will be processed as usual, and the resulting configuration will be
   printed. No actual installation will be performed.

**-i**, **--root-git-user** *<user>*

   Set root's username for Git (defaults to `root`). This is only relevant if the `git`
   package is installed in the chroot. It is meant to configure `git` for the root user
   in case `etcgit` is used. However, the configuration will always be performed unless
   disabled with the *--no-git-setup* option.

**-I**, **--root-git-mail** *<mail>*

   Set the email address used by `git` for the `root` user. Defaults to
   `root@localhost`. See *--root-git-user* for more information.

**-j**, **--hostname** *<name>*

   Set the chroot's hostname. Defaults to the name of the chroot's root directory, or
   its parent directory if the root directory's name is `rootfs`. This will be written
   to the chroot's `/etc/hostname` file and added to its `/etc/hosts` file, unless it
   is set to `-`.

**-J**, **--chroot-id** *<name>*

   This defaults to the hostname (as described above), and if it is not set to `-` it
   will be written to the chroot's `/etc/chroot_id` file. This may be useful to include
   it in a shell prompt.

**-k**, **--keep-orphans**

   When syncing the configuration, if a file is found that does not exist on the host,
   and the owning package can not be determined, it will be deleted by default. Use
   this option to keep such files.

**-K**, **--no-keep-orphans**

   The opposite of *--keep-orphans*, see above. This is the default behaviour.

**-l**, **--mirrorlist**

   Copy the `pacman` mirrorlist from the host to the chroot. This is the default.

**-L**, **--no-mirrorlist**

   Do not copy the mirrorlist from the host to the chroot.

**-m**, **--keep-empty**

   After syncing the configuration, empty directories in the chroot's `/etc/` will be
   deleted by default. Use this option to keep them.

**-M**, **--no-keep-empty**

   After syncing, delete empty directories in the chroot's `/etc/`. This is the
   default.

**-n**, **--sync**

   Sync the configuration (i.e. `/etc/`) of the chroot with the host, as described
   before. This is the default.

**-N**, **--no-sync**

   Do not sync the configuration with the host.

**-o**, **--check-mount**

   See `pacstrap`'s *-d* option. By default, **arch-mklxc** will allow installation to
   a non-mountpoint directory (contrary to `pacstrap` defaults). Use this option to
   cancel installation if the chroot's root directory is not a mount point.

**-O**, **--no-check-mount**

   Allow installation if the chroot's root directory is not a mount point. This is the
   default.

**-p**, **--host-cache**

   Use `pacman`'s package cache on the host. This is the default.

**-P**, **--no-host-cache**

   Use the package cache in the chroot.

**-q**, **--skel2root**

   After syncing the configuration, copy the chroot's `/etc/skel/` contents to its
   `/root/` directory. This is the default.

**-Q**, **--no-skel2root**

   Do not copy the `/etc/skel/` contents to `/root/` in the chroot.

**-r**, **--rm** *<file>*

   Remove these files and directories from the chroot. Uses `rm -rf --one-file-system`!
   Paths must be full paths relative to the chroot's root directory.

**-R**, **--no-rm** *<file>*

   When syncing, if a file is not present on the host it will be deleted from the
   chroot if it belongs to a package installed on the host and in the chroot. Use this
   option to exclude files from being deleted. This option may also be used to exclude
   empty directories from being deleted. Files (or directories) must be specified
   relative to the `/etc/` directory. This option may be specified multiple times, and
   may contain shell glob patterns. See also *--skip*.

**-s**, **--setup**

   Configure the chroot after installation. This is the default.

**-S**, **--no-setup**

   Do not configure the chroot after installation, exit after `pacstrap`.

**-t**, **--git-setup**

   If the `git` package is installed in the chroot, configure `git` for the root user.
   See *--root-git-user* and *--root-git-mail*. This is the default.

**-T**, **--no-git-setup**

   Do not configure `git` for the root user in the chroot.

**-u**, **--user** *<user[:<uid>]>*

   Create a regular user in the chroot, with the specified username. If this is not
   specified, a user with the same name as the host's user with the user ID `1000` will
   be created in the chroot. To change the user ID of this user in the chroot, append
   the desired ID to the username, separated by a colon (`:`). To not create a regular
   user in the chroot, set the username to `nobody`.

**-U**, **--shell** *<shell>*

   Set the login shell of the main user in the chroot. No default value, depends on the
   chroot's configuration.

**-v**, **--version**

   Print version information.

**-V**, **--opts-avail**

   Developer only option. You may safely ignore it.

**-w**, **--resolv-conf**

   Copy nameserver entries from the host to the chroot. This is the default. Note that
   if `resolv.conf` in the chroot is a sybolic link this step will be skipped!

**-W**, **--no-resolv-conf**

   Do not copy the nameserver entries from the host to the chroot.

**-x**, **--exclude** *<package>*

   Exclude a package from installation. This option may be specified multiple times.
   The default exclusion list contains the following packages: `jfsutils`, `linux`,
   `netctl`, `reiserfsprogs`, `xfsprogs`.

**-X**, **--etcgit-exclude** *<file>*

   The exclusion file for `etcgit` to be copied to the chroot (ignored if `etcgit` is
   not installed in the chroot). The default is `/root/.etcgit/info/exclude`.

**-y**, **--check-empty**

   Check if the chroot's root directory is empty, and exit with an error if it is not.
   This is the default behaviour.

**-Y**, **--no-check-empty**

   Do not exit if the chroot's root directory is not empty. Use with care!

**-z**, **--mask** *<service>*

   Mask a systemd service in the chroot. This option may be specified multiple times.

**-Z**, **--default-target** *<target>*

   Set systemd's default target in the chroot. Defaults to `multi-user.target`.

**-0**, **--timezone** *<timezone>*

   Timezone to set inside the chroot, e.g. `Europe/Berlin`. Defaults to the timezone
   used by the host.

**-1**, **--machine-id**

   Generate a machine ID inside the chroot. This is the default.

**-2**, **--no-machine-id**

   Do not generate a machine ID inside the chroot.

**-3**, **--locale-gen**

   Run `locale-gen` inside the chroot. This is the default.

**-4**, **--no-locale-gen**

   Do not run `locale-gen` inside the chroot.

**-5**, **--ldconfig**

   Run `ldconfig` inside the chroot. This is the default.

**-6**, **--no-ldconfig**

   Do not run `ldconfig` inside the chroot.

**-7**, **--group <group>**

   Create the specified group as a system group inside the chroot, and add the main
   user to that group (if a main user is created). This option may be specified
   multiple times.

**-8**, **--passwords**

   Set the passwords of root and the main user inside the chroot. The passwords will be
   set to the username (i.e. `root` for root, and the username of the main user for the
   main user). This is the default behaviour. MAKE SURE TO CHANGE THE PASSWORDS AFTER
   THE CHROOT HAS BEEN CREATED!

**-9**, **--no-passwords**

   Do not set the passwords for root and the main user. Login may not be possible if
   this options is set.

**-+**, **--mkdir** *<[<mode>:]directory>*

   Create a directory in the chroot. It must be specified by its full path relative to
   the chroot's root directory. The directory may be prefixed by a (numerical) mode,
   followed by a colon (`:`), to create the directory with the specified permissions.
   Parent directories will be created as required (the permissions will be set for
   them, too). Owner will always be `root:root`. This option may be specified multiple
   times.

**-%**, **--chown** *<user:group:file>*

   Change the owner of the specified file or directory (recursively). This option may
   be specified multiple times. The specified use and group must exist inside the
   chroot.

**-[**, **--usr-local-sbin**

   Link `/usr/local/sbin/` to `/usr/local/bin/` inside the chroot. This is the default
   behaviour.

**-]**, **--no-user-local-sbin**

   Do not link `/usr/local/sbin/` to `/usr/local/bin/` inside the chroot.

CONFIGURATION FILE
=======================================================================================

A configuration file may be specified using the **-f** (or **--config**) option. In
this file, any valid command line option may be specified, in the following format:

   `<option>[ <value>]`

There must be one option per line. The `<option>` must be the long name of the command
line option to set, without any leading dashes. `<option>` and `<value>` must be
separated by at least one space character, options that do not require an argument must
appear alone on one line. Empty lines and lines starting with `#` will be ignored.

The configuration file will be processed at the position it appears on the command
line, i.e. any command line options set before **-f** will be overridden by values in
the configuration file, and later options will override options from the configuration
file (for lists values will be appended, see the *OPTIONS* section for more details).

The special option name `packages` may be used to specify a list of packages to install
(separated by space or comma). As with all lists, it may be specified multiple times.

It is possible to include another configuration file from within a configuration file,
there is no check for infinite loops, though!

Example configuration file:

::

   # This is a comment.

   exclude        cryptsetup
   exclude        device-mapper

   user           nobody

   no-git-setup
   no-passwords

   packages       base,nginx

LICENSE
=======================================================================================

Copyright © 2017-2018 Stefan Göbel < archlxc ʇɐ subtype ˙ de >.

**arch-mkchroot** is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

**arch-mkchroot** is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
**arch-mkchroot**. If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=87: