#!/bin/bash

# The xorg module provides some settings to allow X applications to be run in the container.
#
# The fontconfig and ttf-dejavu packages will be added automatically (a TT font is required in most cases anyway,
# we just make sure to use DejaVu instead of what would be the default when no font is set).
#
# Creating the xhost snippet will be done in the config stage. Note: If the default path is overridden on the
# command line the file (and maybe its parent directories if these have to be created) will be owned by root and
# ownership has to be changed manually! When left to the defaults the owner will be changed to the host's main
# user (the snippet will be created as <home directory>/.config/xinitrc.d/xhost-<user>.sh, it is up to the user to
# make sure this file will be executed when required).
#
# As an alternative (or in addition) to the xhost options this module also provides an option to bind mount the
# host user's ~/.Xauthority into the conatiner. The file's permissions will not be modified, if the container user
# does not have access it has to be granted manually if required.
#
# During prepare, the following variables will be set:
#
#     $xorg_xauth             .Xauthority path if set on the command line, "@@XORG_XAUTH@@" if no path is set, or
#                             empty if the option is not enabled. Set to the actual value (and made ro) during the
#                             config stage.
#
# During config, the following variables will be set:
#
#     $xorg_xhost_snippet     Location of the xhost snippet, or empty for none. Readonly.
#
###################################################################################################################

xorg_main() {

   case "$1" in

      depends ) printf 'user\n' ;;
      options ) xorg_options    ;;
      prepare ) xorg_prepare    ;;
      config  ) xorg_config     ;;

   esac

}

###################################################################################################################

xorg_options() {

   cat <<'______EOF'
      xauthority-bind<file>?     Bind mount the host user's .Xauthority into the container.
      xhost-snippet<file>?       Create a shell script with "xhost +si:localuser:…".
      xhost-user<user>           Allow access for this user instead of the default.
______EOF

}

###################################################################################################################

xorg_prepare() {

   # Include xorg.conf arch-mkchroot and LXC configuration if it exists:

   add_mklxc_include
   add_mkchroot_conf

   # Add some packages:

   mkcrt_pkg+=( 'fontconfig' 'ttf-dejavu' )

   # Add the bind mount if required. Note that we have to use placeholders for the actual files' locations here,
   # since some of the variables will only be available after installation (at which point we couldn't just add the
   # option to the command line arguments anymore). The placeholders will be replaced during the config stage.

   declare -g xorg_xauth=''

   if mbs_opt xauthority-bind ; then

      xorg_xauth=$( mbs_val xauthority-bind )

      if [[ -z "$xorg_xauth" ]] ; then
         xorg_xauth='@@XORG_XAUTH@@'
      fi

      mklxc_opt+=( '--lxc-config' "mount.entry=$xorg_xauth @@XORG_TARGET@@ none bind,optional,create=file,ro" )

   fi

}

###################################################################################################################

xorg_config() {

   # Replace the .Xauthority paths in the config file if required: ================================================

   if mbs_opt xauthority-bind ; then

      declare -g xorg_xauth

      if [[ "$xorg_xauth" == '@@XORG_XAUTH@@' ]] ; then

         xorg_xauth="$home_hst${home_hst:+/.Xauthority}"
         readonly xorg_xauth

         if [[ -z "$xorg_xauth" ]] ; then
            printf "No home directory set for the host's main user.\\n" >&2
            return 1
         fi

         sed -i "s!@@XORG_XAUTH@@!$xorg_xauth!" "$cntr_conf"

      fi

      local _target="${home_cnt:1}${home_cnt:+/.Xauthority}"

      if [[ -z "$_target" ]] ; then
         printf "No home directory set for the container's main user.\\n" >&2
         return 1
      fi

      sed -i "s!@@XORG_TARGET@@!$_target!" "$cntr_conf"

   fi

   # Create the xhost snippet: ====================================================================================

   local _usr=$usr_mirr

   if mbs_opt xhost-user ; then
      _usr=$( mbs_val xhost-user )
   fi

   if [[ -z "$_usr" ]] ; then
      return 0
   fi

   declare -g xorg_xhost_snippet=''
   local      _xhost_snippet_own=''

   if mbs_opt xhost-snippet ; then
      xorg_xhost_snippet=$( mbs_val xhost-snippet )
      if [[ -z "$xorg_xhost_snippet" && -n "$home_hst" ]] ; then
         xorg_xhost_snippet="$home_hst/.config/xinitrc.d/xhost-$_usr.sh"
         _xhost_snippet_own="$usr_host"
      fi
   fi

   readonly xorg_xhost_snippet

   if [[ -z "$xorg_xhost_snippet" ]] ; then
      return 0
   fi

   mkdir -p "$( dirname "$xorg_xhost_snippet" )"
   xorg_rm_parent() { rmdir --ignore-fail-on-non-empty "$( dirname "$xorg_xhost_snippet" )" ; }
   mbs_register_error_handler xorg_rm_parent

   sed 's/^      //' >>"$xorg_xhost_snippet" <<______EOF
      # Allow the user '$_usr' to access the X server:
      #
      xhost +si:localuser:$_usr
______EOF

   xorg_rm_snippet() { rm -f "$xorg_xhost_snippet" ; }
   mbs_register_error_handler xorg_rm_snippet

   chmod +x "$xorg_xhost_snippet"

   if [[ -n "$_xhost_snippet_own" ]] ; then
      chown "$_xhost_snippet_own:$_xhost_snippet_own" "$xorg_xhost_snippet"
      DISPLAY=':0' runuser -u "$_xhost_snippet_own" -- xhost "+si:localuser:$_usr" || true
   fi

}

###################################################################################################################

mbs_readonly_f xorg

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=115: ############################################