# Maintainer: Stefan Göbel < archlxc ʇɐ subtype ˙ de >

pkgname='arch-lxc-scripts'
pkgver='0.2'
pkgrel='1'
arch=( 'any' )
pkgdesc='Miscellaneous scripts to create Arch Linux chroots/LXC containers.'
url='https://gitlab.com/goeb/arch-lxc-scripts/'
license=( 'GPL3' )
depends=( 'arch-install-scripts' )
optdepends=(
   'lxc: For the containers.'
   'modular-bash-scripts: For mkarchlxc.'
   'python-lxc: Required for arch-mklxc etc.'
)
makedepends=( 'python-docutils' )
backup=(
   'etc/lxc/config/base.conf'
   'etc/lxc/config/dhcp.conf'
   'etc/lxc/config/nvidia.conf'
   'etc/lxc/config/sound.conf'
   'etc/lxc/config/video.conf'
   'etc/lxc/config/xorg.conf'
   'etc/lxc/seccomp/common.seccomp'
)

_pkgbuild_dir=${_pkgbuild_dir:-$PWD}
_source_files=(
   'config'
   'doc'
   'modules'
   'service'
   'arch-mkchroot'
   'arch-mklxc'
   'LICENSE'
   'lxc-getconfig'
   'lxc-statehooks'
   'Makefile'
   'mkarchlxc'
   'PKGBUILD'
   'README'
)

prepare() {

   local _dest="$srcdir/$pkgname"
   local _file=''

   mkdir -p "$_dest"

   for _file in "${_source_files[@]}" ; do
      cp -avx "$_pkgbuild_dir/$_file" "$_dest/"
   done

   cd "$_dest"
   make clean

}

build() {

   cd "$srcdir/$pkgname"
   make

}

package() {

   cd "$srcdir/$pkgname"
   make install DESTDIR="$pkgdir" PREFIX='/usr'

}

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=78: