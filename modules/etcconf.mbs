#!/bin/bash

# If the --etcconf-dir directory (defaults to /etc/lxc/containers/) exists the container configuration file will be
# moved there (as <container name>.config, any existing files will be overwritten) and a symlink will be created in
# the container directory (in the config stage). During prepare, the following variables will be set:
#
#     $etcconf_dir   Container config directory in /etc (--etcconf-dir value or /etc/lxc/containers).
#
# Note that specifying a non-existing directory will not cause an error, the config file will just be left in the
# container directory.
#
# If etcconf.conf configuration files exist for arch-chroot or LXC, they will be included during prepare.
#
# The $cntr_conf variable will not be modified and point to the symlink after this module's config function has
# been run.
#
###################################################################################################################

etcconf_main() {

   case "$1" in

      depends ) printf 'simplify\n' ;;
      options ) etcconf_options     ;;
      prepare ) etcconf_prepare     ;;
      config  ) etcconf_config      ;;

   esac

}

###################################################################################################################

etcconf_options() {

   cat <<'______EOF'
      etcconf-dir<directory>     Move container config to this directory [/etc/lxc/containers].
______EOF

}

###################################################################################################################

etcconf_prepare() {

   declare -g etcconf_dir='/etc/lxc/containers'

   if mbs_opt etcconf-dir ; then
      etcconf_dir=$( mbs_val etcconf-dir )
   fi

   add_mklxc_include
   add_mkchroot_conf

}

###################################################################################################################

etcconf_config() {

   readonly etcconf_dir

   if [[ -d "$etcconf_dir" ]] ; then
      mv -f "$cntr_conf" "$etcconf_dir/$cntr_name.config"
      ln -s "$etcconf_dir/$cntr_name.config" "$cntr_conf"
   fi

}

###################################################################################################################

mbs_readonly_f etcconf

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=115: ############################################