CNFDIR ?= /etc/lxc
PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
SHRDIR ?= $(PREFIX)/share/arch-lxc-scripts
MODDIR ?= $(SHRDIR)/mkarchlxc/modules
DOCDIR ?= $(PREFIX)/share/doc/arch-lxc-scripts
MANDIR ?= $(PREFIX)/share/man
SRVDIR ?= $(PREFIX)/lib/systemd/system

doc: doc/arch-mkchroot.8 doc/lxc-getconfig.8

clean:

	rm -f doc/arch-mkchroot.8
	rm -f doc/lxc-getconfig.8

install: doc

	# Executables etc.:

	install -d $(DESTDIR)$(BINDIR)
	install -t $(DESTDIR)$(BINDIR)      -m 755 arch-mkchroot
	install -t $(DESTDIR)$(BINDIR)      -m 755 arch-mklxc
	install -t $(DESTDIR)$(BINDIR)      -m 755 lxc-getconfig
	install -t $(DESTDIR)$(BINDIR)      -m 755 lxc-statehooks
	install -t $(DESTDIR)$(BINDIR)      -m 755 mkarchlxc

	# Shared files:

	install -d $(DESTDIR)$(MODDIR)
	install -t $(DESTDIR)$(MODDIR)      -m 755 modules/*.mbs

	# Documentation:

	install -d $(DESTDIR)$(DOCDIR)
	install -t $(DESTDIR)$(DOCDIR)      -m 644 README

	install -d $(DESTDIR)$(MANDIR)/man8
	install -t $(DESTDIR)$(MANDIR)/man8 -m 644 doc/arch-mkchroot.8
	install -t $(DESTDIR)$(MANDIR)/man8 -m 644 doc/lxc-getconfig.8

	# Service files etc.:

	install -d $(DESTDIR)$(SRVDIR)
	install -t $(DESTDIR)$(SRVDIR)      -m 644 service/lxc-statehooks.service

	# Configuration:

	install -d $(DESTDIR)$(CNFDIR)
	cp -avx -t $(DESTDIR)$(CNFDIR)             config/config
	cp -avx -t $(DESTDIR)$(CNFDIR)             config/seccomp

uninstall:

	rm -f  $(DESTDIR)$(BINDIR)/arch-mkchroot
	rm -f  $(DESTDIR)$(BINDIR)/arch-mklxc
	rm -f  $(DESTDIR)$(BINDIR)/lxc-getconfig
	rm -f  $(DESTDIR)$(BINDIR)/lxc-statehooks

	rm -rf $(DESTDIR)$(SHRDIR)
	rm -rf $(DESTDIR)$(DOCDIR)

	rm -f  $(DESTDIR)$(MANDIR)/man8/arch-mkchroot.8
	rm -f  $(DESTDIR)$(MANDIR)/man8/lxc-getconfig.8

	rm -f  $(DESTDIR)$(SRVDIR)/lxc-statehooks.service

doc/arch-mkchroot.8: doc/arch-mkchroot.8.rst
	rst2man doc/arch-mkchroot.8.rst >doc/arch-mkchroot.8

doc/lxc-getconfig.8: doc/lxc-getconfig.8.rst
	rst2man doc/lxc-getconfig.8.rst >doc/lxc-getconfig.8

.PHONY: clean doc install uninstall

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=87: