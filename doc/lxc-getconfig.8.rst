=======================================================================================
lxc-getconfig
=======================================================================================

---------------------------------------------------------------------------------------
Get configuration values of an LXC container.
---------------------------------------------------------------------------------------

:Author:         Stefan Göbel < archlxc ʇɐ subtype ˙ de >
:Date:           2018/11/20
:Version:        0.2
:Manual section: 8
:Manual group:   Admin Commands

SYNOPSIS
=======================================================================================

**lxc-getconfig** *[options]* *<key>…*

DESCRIPTION
=======================================================================================

**lxc-getconfig** basically does the same as `lxc-info <container> -c <key>` (the
output is a bit different for certain values), i.e. it retrieves the configured value
of the specified setting of an LXC container.

Unfortunately, `lxc-info` does block when used in a pre-start hook, which will in turn
block the container start. This script may be used instead to work around that. The
drawback is the requirement of LXC's Python bindings!

OPTIONS
=======================================================================================

-l, --lines                For lists, print one element per line (only works if `-v` is
                           used, too). Otherwise the whole list will be printed in
                           Python's standard format.

-n, --name <container>     The name of the container. This is required. Unlike current
                           versions of the `lxc-*` tools, the `-n` must not be omitted!

-v, --value                Do not include the option name, followed by an equal sign
                           embedded in spaces, print only the actual configuration
                           value. For list values, this option is required for `-l` to
                           take effect.

The configuration setting's *<key>* must follow the options. More than one key may be
specified, they will be looked up and printed one per line (excpet for lists with `-l`
and `-v`).

SEE ALSO
=======================================================================================

*lxc.container.conf*\ (5), *lxc-info*\ (1)

LICENSE
=======================================================================================

**lxc-getconfig** is part of **arch-lxc-scripts**.

Copyright © 2017-2018 Stefan Göbel < archlxc ʇɐ subtype ˙ de >.

**arch-lxc-scripts** is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later version.

**arch-lxc-scripts** is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
**arch-lxc-scripts**. If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=87: